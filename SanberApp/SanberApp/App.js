import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons'
import { Ionicons } from '@expo/vector-icons'
import VideoItem from './components/videoItem';
import data from './data.json'
import { 
  SafeAreaView,
  StyleSheet, 
  Text, 
  View , 
  Image,
  Dimensions,
  TouchableOpacity


} 
from 'react-native';
console.log(Dimensions.get("screen")); // <== Cara Mengetahui Ukuran LIVE Device/Emulator (POCOPHONE F1)
export default class App extends Component {
  render() {
    
  return (
    <View style={styles.container}>
      <View style={styles.navBar}>
        <Image
        source={require('./assets/logo.png')} 
        style={{width:96, height:22}} 
        />
        <View style={styles.rightNav}>
          <TouchableOpacity>
          <Icon style={styles.btn} name='search' size={25} color='grey' />
          </TouchableOpacity>
          <TouchableOpacity>
          <Icon name='account-circle' size={25} color='#f4f4f4' />
          </TouchableOpacity>

        </View>
      </View>     
      <View style={styles.body}>
        <VideoItem video={data.items[0]}>



        </VideoItem>
      </View>
      <View style={styles.tabBar}>
        <TouchableOpacity style={styles.tabItem}>
          <Icon name='home' size={25} color='grey'/>
          <Text style={styles.tabTitle}>Home</Text> 
        </TouchableOpacity>

        <TouchableOpacity style={styles.tabItem}>
          <Icon name='whatshot' size={25} color='grey'/>
          <Text style={styles.tabTitle}>Trending</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.tabItem}>
          <Icon name='subscriptions' size={25} color='grey'/>
          <Text style={styles.tabTitle}>Subscription</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.tabItem}>
          <Icon name='folder' size={25} color='grey'/>
          <Text style={styles.tabTitle}>Library</Text>
        </TouchableOpacity>


      </View>
      
    </View>
  );
}
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  navBar: {
    height: 75,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 15,
    paddingTop: 30,
    flexDirection : 'row',
    alignItems: "center",
    justifyContent: 'space-between'
  },
  rightNav : {
    flexDirection:'row'
  },
  btn: {
    right:10,
  },
  body: {
    flex: 1
  },
  tabBar: {
    backgroundColor: 'white',
    height:60,
    borderTopWidth: 0.5,
    borderColor: '#e5e5e5',
    flexDirection : 'row',
    justifyContent: 'space-around'
    
  },
  tabItem: {
    alignItems:'center',
    justifyContent:'center',

  },

  tabTitle: {
    fontSize: 12,
    color: '#3c3c3c',
    paddingTop : 2
    
  },


}) ;
  