// Class Method
console.log("<><><- - - - - Declare and Ekspresi Method - - - - - -><><>")

class MainContent {
    constructor (recommended){
        this.rec=recommended
    }
    present(x){
        return "Game " + this.rec + " masuk sebagai rekomendasi Game" + x ;
    }
}
newrec = new MainContent("PUBG Mobile")
console.log(newrec.present("Online"));

// Static Method
console.log("<><><- - - - - Static Method - - - - - -><><>")

class StaticTest {
    constructor(brand) {
      this.carname = brand;
    }
    static hello() {
      return "Hello!!";             // <------------------- Static
    }
  }
  
  mycar = new StaticTest("Ford");
  
  // memanggil 'hello()' pada class Car:
  console.log(StaticTest.hello());
  
  // dan tidak bisa pada 'mycar':
  // console.log(mycar.hello());
  // jika menggunakan sintaks tersebut akan memunculkan error.

  console.log("<><><- - - - - Inheritance - - - - - -><><>")

  class Car {
    constructor(brand) {
      this.carname = brand;
    }
    present() {
      return 'I have a ' + this.carname;
    }
  }
  
  class Model extends Car {
    constructor(brand, mod) {
      super(brand);
      this.model = mod;
    }
    show() {
      return this.present() + ', it is a ' + this.model;
    }
  }
  
  mycar = new Model("Ford", "Mustang");
  console.log(mycar.show());

  console.log("<><><- - - - - Getters and Setters - - - - - -><><>")

  class Get {
    constructor(brand) {
      this.carname = brand;
    }
    get cnam() {
      return this.carname;
    }
    set cnam(x) {
      this.carname = x;
    }
  }
  
  mycar = new Get("Test");
  console.log(mycar.cnam); // Ford

  // getter cnam digunakan tanpa "()"