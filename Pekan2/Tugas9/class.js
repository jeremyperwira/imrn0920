// Animal , Property ; Legs=(4) , ColdBlooded(false)
// < - - - - NOMOR 1 - - - - >
console.log(` < < < NOMOR 1 > > > `)

class Animal {
    // Code class di sini
    constructor (name){
        this.cnam = name;
        this.legs = 4;
        this.cold_blooded = false
    }
    get cnam(){
        return this.name
    }
    set cnam(x){
        this.name = x
    }

}

var sheep = new Animal("shaun");
 
console.log(sheep.cnam) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// < - - - - NOMOR 1.2 - - - - >
// < inheritence from Number Uno >
console.log(` < < < NOMOR 2 > > > `)

class Ape extends Animal{
    constructor(sungokong){
        this.apesound = sungokong
        this.frogsound = kodok
    }
}
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 



// < - - - - NOMOR 2 - - - - >
// < Function to Class >
console.log(` < < < NOMOR 2 > > > `)
/*
function Clock({ template }) {
  
    var timer;
  
    function render() {
      var date = new Date();
  
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      var output = template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    this.stop = function() {
      clearInterval(timer);
    };
  
    this.start = function() {
      render();
      timer = setInterval(render, 1000);
    };
  
  }
  
  var clock = new Clock({template: 'h:m:s'});
  clock.start(); 
*/
// Convert Class

class Clock {
  constructor(hour,mins,secs){
    this.hour=hour
    this.mins=mins
    this.secs=secs
    this.stop = function() {
      clearInterval(timer);
    };
  
    this.start = function() {
      render();
      timer = setInterval(render, 1000);
    };
  }
    start(){
      return this.start
    }
}

var clock = new Clock({template: 'h:m:s'});
console.log(clock.start());  