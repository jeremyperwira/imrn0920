// class Car {
//     constructor(brand) {
//       this.carname = brand;
//     }
//     present() {
//       return "I have a " + this.carname;
//     }
//   }
  
//   mycar = new Car("Ford");
//   console.log(mycar.present()) // I have a Ford

  class Car {
    constructor(brand) {
      this.carname = brand;
    }
    static hello() {
      return "Hello!!";
    }
  }
  
  mycar = new Car("Ford");
  
  // memanggil 'hello()' pada class Car:
  console.log(Car.hello());
  
  // dan tidak bisa pada 'mycar':
  // console.log(mycar.hello());
  // jika menggunakan sintaks tersebut akan memunculkan error.

