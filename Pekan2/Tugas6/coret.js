// File untuk latihan Javascript //
// Created by Jeremy Perwira //

console.log("- - - - - 1. Mendeclare sebuah Object dengan 'Key' dan 'Value'- - - - - ")

var identitas= {
    namadep : "Budi", // "namadep" berperan sebagai fungsi dan "Budi" sebagai value dari sebuah object
    namabel : "Suandi",
    alamat : "Jalan-Jalan jadian Kaga",
    hobi : "Ngerusuh",
    umur : 19,
    keterangan : "Contoh console.log dalam var identitas",
    "tempat lahir" : "jalan sepi" , // Untuk "Key" yang >1 kata mengunakan tanda petik ("double key")
}
console.log(identitas.keterangan)

console.log("- - - - - 2. Mendeclare serta menambah Object dengan 'Key' dan 'Value'- - - - - ")

var kosong = {}
// Var object yang kosong dapat ditambah dengan mengassign key dan value nya ke property tersebut 
kosong.isi= "Ini sudah disi",
kosong["double key"] = "Terdapat dua karakter key "

console.log(kosong.isi)
console.log(kosong)

console.log("- - - - - 3. Mengaskses nilai pada Object dengan 'Key' dan 'Value'- - - - - ")

var akses = {
    pertama : "Kamu Mengakses Key pertama dengan object key pertama",
    kedua : "Kamu Mengakses Key kedua dengan object kedua",
    ketiga : "Kamu Mengakses Key ketiga dengan object ketiga",
    keempat : "Kamu Mengakses Key keempat dengan metode array atau kurung-siku"
}
console.log(akses.pertama) // Kamu ..... object key "Pertama"
console.log(akses["keempat"])



console.log("- - - - - 4. Perbedaan Array dan Object- - - - - ")
// Tipe data Array technically adalah sebuah Object tetapi Array memiliki sifat khusus. Array secara otomatis
//  memberikan indeks yang analogi dengan key pada Object. Coba kamu cek di console menggunakan typeof

var array = [ 1, 2, 3 ] 
console.log(typeof array) // object

console.log("- - - - - 5. Area LATIHAN Tugas - - - - - ")


