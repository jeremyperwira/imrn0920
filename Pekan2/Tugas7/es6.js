
// NOMOR 1 //
// Convert to Arrow Function //
const golden = function goldenFunction(){
    console.log("this is golden!!")
  }
   
  golden()

// Arrow Function //
goldenFunction = () => {
    console.log("This is Golden!! In Arrow Mode")
}
    goldenFunction();

/* - - - - - - - - - - - - - - - - - - - - - - - - - */
// NOMOR 2 //
// Object Literal ES5 to ES6 //



const newFunction = (firstName, lastName) => {
    return {
      firstName, lastName,
      fullName(){
          console.log(firstName+ ' ' + lastName)
          return
    
      }
    }
  }

  //Driver Code 
  newFunction("William", "Imoh").fullName() 

/* - - - - - - - - - - - - - - - - - - - - - - - - - */
// NOMOR 3 //
// Destructing //

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

const {firstName,lastName,destination,occupation,spell} = newObject

// Driver code
console.log(firstName, lastName, destination, occupation)

/* - - - - - - - - - - - - - - - - - - - - - - - - - */
// NOMOR 4 //
// Array Spreading //
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]


let combined = [...west,...east]

//Driver Code
console.log(combined)


/* - - - - - - - - - - - - - - - - - - - - - - - - - */
// NOMOR 4 //
// Templated  //
const planet = "earth"
const view = "glass"
var before = `Lorem '  ${view} + 'dolor sit amet, ' +  
'consectetur adipiscing elit,'  ${planet} + 'do eiusmod tempor ' +
'incididunt ut labore et dolore magna aliqua. Ut enim' +
' ad minim veniam`

// Driver Code
console.log(before) 