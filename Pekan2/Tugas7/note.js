
console.log("Normal Let + Const Javascript")
//~~~~~~~~~~~~~~~~~~~~ Normal Javascript ~~~~~~~~~~~~~~~~~~~~  //

var x = 1;
 
if (x === 1) {
var x = 2;
 
console.log(x);
// expected output: 2
}
 
console.log(x); // 2 


console.log("ES6 Let + Const")
//~~~~~~~~~~~~~~~~~~~~  ES6 ~~~~~~~~~~~~~~~~~~~~ //

let y = 1;
 
if (y === 1) {
  let y = 2;
 
  console.log(y);
  // expected output: 2
}
 
console.log(y); // 1 

const number = 42;
// number = 42; // Uncaught TypeError: Assignment to constant variable. 