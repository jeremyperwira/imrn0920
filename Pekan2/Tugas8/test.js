// Deklarasi function yang memilik callback sebagai parameter
console.log(`!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Callback (Mendeclare dan Menajalankan Callback) ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ !`)
function periksaDokter(nomerAntri, callback) {
    if(nomerAntri > 50 ) {
        callback(false)
    } else if(nomerAntri < 10) {
        callback(true)
    }    
} 
// Menjalankan function periksaDokter yang sebelumnya sudah dideklarasi
periksaDokter(65, function(check) {
    if(check) {
        console.log("sebentar lagi giliran saya")
    } else {
        console.log("saya jalan-jalan dulu")
    }
}) 
//
console.log(` ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Promise (! Mendeclare dan Menajalankan Promise !)~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ `)
var isMomHappy = false;
 
// Promise
var willIGetNewPhone = new Promise(
    function (resolve, reject, noanswer) {
        if (isMomHappy) {
            var phone = {
                brand: 'Samsung',
                color: 'black'
            };
            resolve(phone); // fulfilled atau janji dipenuhi
        } 
        
        else {
            var reason = new Error('askmom() response => mom is not happy');
            reject(reason); // reject (ingkar)
        } 
        
 
    }
);

function askMom() {
    willIGetNewPhone
        .then(function (fulfilled) {
            // yay, you got a new phone
            console.log(fulfilled);
         // output: { brand: 'Samsung', color: 'black' }
        })
        .catch(function (error) {
            // oops, mom don't buy it
            console.log(error.message);
         // output: 'mom is not happy'
        });
}
 
// Tanya Mom untuk menagih janji
askMom() 

console.log(`CATATAN : Untuk menagih janji dibuat sebuah function dengan nama askMom yang isinya adalah menagih janji 
willIGetNewPhone. Ketika anak menagih janji menggunakan function askMom() maka promise willIGetNewPhone dipanggil 
dan terdapat dua methods yaitu then dan catch . Method .then() dan .catch() keduanya menerima parameter function. 
Resolve yang dijalankan di pendeklarasian promise akan mengirim handphone baru dan ditangkap di method .then(). 
Sedangkan reject pada pendeklarasian promise akan mengirim pesan error atau alasan kenapa janji diingkari dan 
ditangkap di method .catch().`)




