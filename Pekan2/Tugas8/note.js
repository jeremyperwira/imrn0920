const help =`Di dalam dunia pemrograman terdapat dua cara dalam menjalankan program: Synchronous dan Asynchronous.
 Synchronous artinya program berjalan secara berurutan sedangkan Asynchronous artinya program berjalan bersama-sama.
 Terkadang di dalam program yang kita buat terdapat suatu sintaks yang mengharuskan code pada baris tersebut untuk 
 dijalankan terlebih dahulu sebelum menjalankan sintaks pada baris selanjutnya. Hal ini dikenal dengan istilah blocking.
 Sebaliknya non-blocking artinya program berjalan dengan mengeksekusi sintaks dari baris ke baris secara paralel 
 (bersama-sama) .
 
 Perhatikan contoh program di bawah ini:
  ! ----------- ! `
  console.log(help)
  
 setTimeout(function() {
    console.log("saya dijalankan belakangan")
  }, 3000)
   
    console.log("saya dijalankan pertama") 

var help2 = 
  `
  ! ---------- !
  Jika kita jalankan program di atas, maka yang akan tampil terlebih dahulu di console adalah “saya dijalankan 
  pertama” walaupun sintaksnya ditulis belakangan setelah function setTimeout. Function setTimeout di atas merupakan 
  salah satu contoh function asynchronous di Javascript.
  Cara untuk mengatasi Asynchronous seperti function setTimeout adalah dengan !Callback! atau dengan !Promise!.

 `
 console.log(help2)


console.log("! - - - - - - - - - - - - - - - - - - - - - - - - READ THIS - - - - - - - - - - - - - - - - - - - - - - - - !")

