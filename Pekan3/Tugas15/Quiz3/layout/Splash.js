import React, { useEffect } from 'react';
import { StyleSheet, Text, View , Image } from 'react-native';
const Splash = ({navigation}) => {
    useEffect(()=>{
        setTimeout(()=>{
            navigation.replace("Register")
        }, 3000)
    }, [])

    return (
        <View style={styles.container}>
            <View style={{justifyContent:'center', alignItems:'center', flex:1}}>
                <Image source={require('../assets/sanberlogo.png')} />
                <Text style={{fontSize: 30}}>Selamat Datang</Text>
            </View>
        </View>
    )   
}

export default Splash

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'white'
    }
})
