import React from 'react'
import { Button, StyleSheet, Text, View , SafeAreaView} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { color } from 'react-native-reanimated';


// UI Slicing //
const Home = ({navigation}) => {
    return (
        <SafeAreaView style={styles.container}>
            {/* Bagian Deskripsi  */}
            <View style={styles.desc}> 
                <Text style={styles.desc1}>Welcome</Text>
                <Text style={styles.desc2}>Sign Up to Continue</Text>
            </View>
    
            {/* Bagian Tabel  */}
            <View style={styles.kotakkonten}>
                <View style={styles.kontener}>
                    <View style={styles.konten}>
                        <Text style={styles.konten1}> Name </Text>
                        <Text style={styles.konten2}> Shakibul Islam</Text>
                        <View
                            style={{
                                borderBottomColor: '#E6EAEE',
                                borderBottomWidth: 1,
                                marginRight: 32 
                            }}>
                        </View>

                    </View>
                    <View style={styles.konten}>
                        <Text style={styles.konten1}> Email </Text>
                        <Text style={styles.konten2}> Shakibulislam402@gmail.com</Text>
                        <View
                            style={{
                                borderBottomColor: '#E6EAEE',
                                borderBottomWidth: 1,
                                marginRight: 32
                            }}>
                        </View>
                    </View>
                    <View style={styles.konten}>
                        <Text style={styles.konten1}> Phone Number </Text>
                        <Text style={styles.konten2}> +44 213 032 578</Text>
                        <View
                            style={{
                                borderBottomColor: '#E6EAEE',
                                borderBottomWidth: 1,
                                marginRight: 32
                            }}>
                        </View>
                    </View>
                    <View style={styles.konten}>
                        <Text style={styles.konten1}> Password   </Text>
                        <Text style={styles.konten2}> *********</Text>
                        <View
                            style={{
                                borderBottomColor: '#E6EAEE',
                                borderBottomWidth: 1,
                                marginRight: 32
                            }}>
                        </View>
                    </View>
                    
                </View>
                
                <View style={styles.masuk}>
                    <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                        <View style={styles.button}>
                            <Text style={styles.signup}>Sign Up</Text>
                        </View>
                    </TouchableOpacity>
                    <Text style={styles.signup2}> Already have an account? Sign In</Text>
                </View>


            </View>
            
            
            
        </SafeAreaView>
      );
}

export default Home

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#fefefe',
    },
    desc:{
        marginTop: 100,
        paddingLeft: 22.28,
    },
    desc1:{
        fontWeight:'bold',
        fontSize:30,
        fontStyle:"normal",
        color: '#0C0423'
    },
    desc2:{
        fontWeight:'400',
        fontSize:12,
        fontStyle:"normal",
        color: '#4d4d4d'
    },


    kotakkonten:{
        marginLeft: 32,
        marginRight:16,
        marginTop: 32,
        paddingBottom:32,
        backgroundColor: '#ffffff',
        elevation: 8,
        // shadowRadius: 10,
        // shadowColor: 'black',
        borderRadius: 11
    },
    kontener:{
        marginLeft:16,
        marginTop:4,
    },
    konten:{
        marginVertical: 15,
    },
    konten1:{
        fontSize: 12,
        color:'#4d4d4d',
        fontWeight:'100'
    },
    konten2:{
        fontSize: 15,
        color:'#4C475A',
        fontWeight:'100'
    },
    masuk:{
        marginTop:39,
        alignItems:'center'
    },
    button:{
        backgroundColor: '#F77866',
        borderRadius: 6,
        paddingHorizontal: 120,
        paddingVertical: 15
    },
    signup:{
        color:'white',
        fontSize:14,
        fontWeight:'bold'
    },
    signup2:{
        color:'#4d4d4d',
        fontSize:12,
        marginVertical: 12,
    }
})
