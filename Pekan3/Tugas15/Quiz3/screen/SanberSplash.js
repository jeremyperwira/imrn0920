import React, { useEffect } from 'react';
import { StyleSheet, Text, View ,Image} from 'react-native';
const Splash = ({navigation}) => {
    useEffect(()=>{
        setTimeout(()=>{
            navigation.replace("Home")
        }, 3000)
    }, [])

    return (
        <View style={{justifyContent:'center', alignItems:'center', flex:1}}>
            <Image src='./asset/Sanber'></Image>
        </View>
    )
}

export default Splash

const styles = StyleSheet.create({})
