import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { FontAwesome5 } from '@expo/vector-icons';
import { 
  SafeAreaView,
  StyleSheet, 
  Text, 
  View , 
  Image,
  Dimensions,
  TouchableOpacity,
  TextInput,
  Platform,
  Button,
  KeyboardAvoidingView,
  ScrollView,
  


}from 'react-native';
const AboutScreen = () => {
    return (
        <KeyboardAvoidingView
            behavior={Platform.OS == "ios"? "padding":"height"}
            style={styles.container}>
        
            <ScrollView>
                <SafeAreaView style={styles.container}>
                    
                    {/* Avatar, Nama, Skills */}
                    <View style={styles.avatar}>
                        <Icon name='account-circle' size={100} color='grey'/>
                        <Text style={styles.nickname}>Jeremy Perwira</Text>
                        <Text style={[styles.username,{fontWeight:'700'}]}>@jeremyperwira</Text>
                        <Text style={styles.skill}> UI/UX Designer | Software Engineer | Start-Up Founder | Esport Player</Text>
                    </View>


                    {/*Bagian Link/Icon/Thumbnail */}
                    <View style={styles.iconBar}>
                        <TouchableOpacity style={styles.iconset}>
                           <FontAwesome5 name='github-square' size={25} color='grey'/>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.iconset}>
                         <FontAwesome5 name='dribbble-square' size={25} color='grey'/>
                        </TouchableOpacity >
                        <TouchableOpacity style={styles.iconset}>
                          <FontAwesome5 name='twitter-square' size={25} color='grey'/>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.iconset}>
                          <FontAwesome5 name='linkedin' size={25} color='grey'/>
                        </TouchableOpacity> 
                    </View>
                    <View>

                        <View style={styles.listBox}>
                            <View style={styles.ListSkill}>
                            <View>
                                    <Text style={styles.skillName}> UI/UX Designer </Text>
                                        <View style={styles.ContentSkill}>
                                            <TouchableOpacity style={styles.iconskill}>
                                            <FontAwesome5 name='figma' size={40} color='grey'/>
                                            </TouchableOpacity>

                                            <View style={styles.bar}>
                                                <View style={styles.Fullbar}></View>
                                                <View style={styles.Skillbar}></View>
                                            </View>
                                        </View>
                                </View>
                            </View>

                            <View style={styles.ListSkill}>
                                <View>
                                    <Text style={styles.skillName}> React Native Developer </Text>
                                        <View style={styles.ContentSkill}>
                                            <TouchableOpacity style={styles.iconskill}>
                                            <FontAwesome5 name='react' size={40} color='grey'/>
                                            </TouchableOpacity>

                                            <View style={styles.bar}>
                                                <View style={styles.Fullbar}></View>
                                                <View style={styles.Skillbar}></View>
                                            </View>
                                        </View>
                                </View>
                            </View>

                            <View style={styles.ListSkill}>
                                <View>
                                    <Text style={styles.skillName}> Front End Developer </Text>
                                        <View style={styles.ContentSkill}>
                                            <TouchableOpacity style={styles.iconskill}>
                                            <FontAwesome5 name='wordpress' size={40} color='grey'/>
                                            </TouchableOpacity>

                                            <View style={styles.bar}>
                                                <View style={styles.Fullbar}></View>
                                                <View style={styles.Skillbar}></View>
                                            </View>
                                        </View>
                                </View>
                            </View>

                            <View style={styles.ListSkill}>
                                <View>
                                    <Text style={styles.skillName}>Javascript</Text>
                                        <View style={styles.ContentSkill}>
                                            <TouchableOpacity style={styles.iconskill}>
                                            <FontAwesome5 name='java' size={40} color='grey'/>
                                            </TouchableOpacity>

                                            <View style={styles.bar}>
                                                <View style={styles.Fullbar}></View>
                                                <View style={styles.Skillbar}></View>
                                            </View>
                                        </View>
                                </View>
                            </View>
                        </View>
                    </View>

                    
                    
        
                </SafeAreaView>
            </ScrollView>
        </KeyboardAvoidingView>
        
    )
}

export default AboutScreen

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'white',
    },
    avatar:{
        alignItems: 'center',
        paddingTop: 60
    },
    nickname:{
        color:'green',
        fontWeight:'bold'

    },
    username:{

    },
    skill:{
        paddingHorizontal: 40,
        justifyContent: 'space-between',
        textAlign: 'center',

    },
    iconBar:{
        paddingHorizontal: 40,
        paddingTop: 10 ,
        marginHorizontal: 20,
        justifyContent: 'space-evenly',
        // textAlign: 'center',
        flexDirection: 'row'
    },
    ListSkill:{
        textAlign:'center',
        backgroundColor: 'white',
        paddingHorizontal: 20,
        // height: 100,
        paddingBottom: 20,
        marginVertical: 10,
        marginHorizontal:20,
        borderColor: '#f2f2f2',
        borderWidth: 1,
        borderRadius: 8,
        elevation: 4
    },
    skillName:{
        fontSize: 16,
        color: 'green',
        textAlign:'center',
        paddingTop: 4,  
        fontWeight: 'bold'  
    },
    ContentSkill:{
        flexDirection: 'row',
        justifyContent: 'space-between'
        
    },
    bar:{
        justifyContent: 'center'
    },
    Fullbar:{
        height: 15,
        width: 260,
        backgroundColor: '#f3f3f3',
        borderRadius: 8,
        borderColor: 'grey',
        borderWidth: 0.2
    },
    // Skillbar:{
    //     height: 15,
    //     width: 160,
    //     backgroundColor: 'green',
    //     borderRadius: 8,
        
    // }
    
    listBox:{
        marginTop:10,
    }
})
