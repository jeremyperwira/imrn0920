import React, { useState } from 'react'
import { Button, StyleSheet, Text, View } from 'react-native'

const Class = () => {
    const [count, setCount] = useState(0);
    return (
        <View style={{alignItems:'center', justifyContent:'center' , flex:1}}>  
            <Button onPress={()=> setCount(count+1)} title='Tambah'> </Button>
            <Text> {count}</Text>
            <Button onPress={()=> setCount(count-1)} title='Reset'> </Button>

        </View>
    )
}

export default Class

const styles = StyleSheet.create({})
