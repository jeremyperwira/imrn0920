import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { 
  SafeAreaView,
  StyleSheet, 
  Text, 
  View , 
  Image,
  Dimensions,
  TouchableOpacity,
  TextInput,
  Platform,
  Button,
  KeyboardAvoidingView,
  ScrollView,
  


}from 'react-native';
const LoginScreen = () => {
    return (
        <KeyboardAvoidingView
        behavior={Platform.OS == "ios"? "padding":"height"}
        style={styles.container}
        >
        <ScrollView>
            <View style={styles.containerView}>
                <Text style={styles.h1}>Daftar Akun</Text>
                <Text style={styles.h2}>Dengan memiliki akun anda dapat mengakses berbagai fitur yang telah tersedia.</Text>
                <View style={styles.isi}>    
                    <View style={styles.form}>
                        <Text style={styles.formText}>Nama</Text>
                        <TextInput style={styles.input}  />
                    </View>                   
                    <View style={styles.form}>
                        <Text style={styles.formText}>Email</Text>
                        <TextInput  style={styles.input}  />
                    </View>
                    <View style={styles.form}>
                        <Text style={styles.formText}>Nomor Telepon</Text>
                        <TextInput  style={styles.input}  />
                    </View>
                    <View style={styles.form}>
                        <Text style={styles.formText}>Atur Kata Sandi</Text>
                        <TextInput  style={styles.input} secureTextEntry={true}  />
                        <Text style={styles.formText2}>Minimal terdiri dari 8 karakter.</Text>

                    </View>
                    <View style={styles.form}>
                        <Text style={styles.formText}>Konfirmasi Kata Sandi</Text>
                        <TextInput  style={styles.input} secureTextEntry={true}  />
                        <Text style={styles.formText2}>karakter sandi harus sama.</Text>

                    </View>
                </View>
                

                <View style={styles.Pressbtn}>
                    <TouchableOpacity style={styles.btnLogin}>
                        <Text style={styles.btnText}>Daftar</Text>
                    </TouchableOpacity>

                    <TouchableOpacity  style={styles.btnLoginBack}>
                        <Text style={styles.btnText}>Kembali</Text>
                    </TouchableOpacity>
                    <Text style={styles.autotext}>Sudah punya Akun? Masuk</Text>
                </View>

            </View>
            
        
        </ScrollView>
        </KeyboardAvoidingView>
        
    )
}

export default LoginScreen

const styles = StyleSheet.create({
    container : {
        backgroundColor:'white',
        flex:1,
        alignItems:'center',
        paddingHorizontal: 20,
        marginTop: 25,
    },
    h1 :{
        fontSize:24,
        fontStyle: 'normal',
        fontWeight:'bold' ,
        marginTop: 40,
        marginVertical:0,
        color : "#27ae60"
        
    },
    h2 :{
        fontSize:14,
        fontStyle: 'normal',
        fontWeight:'600' ,
        marginVertical:0,
        color : "#333333"
        
    },
    formText:{
        fontSize: 15,
        marginTop:0,
        color: '#bdbdbd'
        

    },
    formText2:{
        fontSize: 12,
        marginTop:0,
        color: '#bdbdbd'
        

    },
    isi :{
        backgroundColor: 'white',
        marginVertical: 10
        
    },
    form :{
        marginTop: 8
    },
    input :{
        height: 40,
        borderColor: '#e0e0e0',
        padding: 10,
        borderWidth: 1,
        borderRadius: 4,
    },
    btnLogin :{
        backgroundColor: '#27ae60',
        height: 52 ,
        alignItems: "center",
        borderRadius: 5,
        padding: 12,
    },
    btnText:{
        textAlign: "center",
        fontSize: 18,
        alignContent: "center",
        color: 'white'
    },
    Pressbtn:{
        paddingTop: 70
    },
    btnLoginBack:{
        backgroundColor: '#bdbdbd',
        height: 52 ,
        alignItems: "center",
        borderRadius: 5,
        padding: 12,
        marginTop: 12 ,
    },
    autotext:{
        marginVertical: 12,
        textAlign: 'center',
        fontSize: 14,
        color: '#bdbdbd',

    },
    
})

