import React, { Component } from 'react'
import { View , Text} from 'react-native'

export default class latihan extends Component {
    state = {test : 0}

    componentDidMount(){
        setInterval(()=>{
            this.setState({test : this.state.test + 1})
        }, 1000) 
    }
    

    render() {
        const {test}= this.state
        return (
            <View style={{alignItems:'center', justifyContent:'center', backgroundColor:'#010101', flex:1}}>
                <Text style={{fontSize: 40 , color : 'white'}}>{test}</Text>
            </View>
        )
    }
}
