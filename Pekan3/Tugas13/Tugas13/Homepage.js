import { StatusBar } from 'expo-status-bar';
import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { 
  SafeAreaView,
  StyleSheet, 
  Text, 
  View , 
  Image,
  Dimensions,
  TouchableOpacity


}from 'react-native';
const Homepage = () => {

  return (
    <View style={styles.container}>
      <View style={styles.navBar}>
        <Text 
            fontWeight='800'
            style={styles.urbanstadia} >
            Urbanstadia
        </Text>
        <View style={styles.rightNav}>
          <TouchableOpacity>
          <Icon style={styles.btn} name='search' size={25} color='grey' />
          </TouchableOpacity>
          <TouchableOpacity>
          <Icon name='account-circle' size={25} color='grey'  />
          </TouchableOpacity>

        </View>
      </View>     
      <View style={styles.body}>
        
      </View>
      <View style={styles.tabBar}>
        <TouchableOpacity style={styles.tabItem}>
          <Icon name='home' size={25} color='grey'/>
          <Text style={styles.tabTitle}>Beranda</Text> 
        </TouchableOpacity>

        <TouchableOpacity style={styles.tabItem}>
          <Icon name='event' size={25} color='grey'/>
          <Text style={styles.tabTitle}>Event</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.tabItem}>
          <Icon name='assessment' size={25} color='grey'/>
          <Text style={styles.tabTitle}>Hasil</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.tabItem}>
          <Icon name='email' size={25} color='grey'/>
          <Text style={styles.tabTitle}>Pesan</Text>
        </TouchableOpacity>


      </View>
      
    </View>
  );
}
export default Homepage


const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  navBar: {
    height: 75,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 15,
    paddingTop: 30,
    flexDirection : 'row',
    alignItems: "center",
    justifyContent: 'space-between'
  },
  urbanstadia:{
      fontSize: 16,
      color: '#27ae60'
  },
  rightNav : {
    flexDirection:'row'
  },
  btn: {
    right:10,
  },
  body: {
    flex: 1,
    backgroundColor: 'red'
  },
  tabBar: {
    backgroundColor: 'white',
    height:60,
    borderTopWidth: 0.5,
    borderColor: '#e5e5e5',
    flexDirection : 'row',
    justifyContent: 'space-around'
    
  },
  tabItem: {
    alignItems:'center',
    justifyContent:'center',

  },

  tabTitle: {
    fontSize: 12,
    color: '#3c3c3c',
    paddingTop : 2
    
  },


}) ;