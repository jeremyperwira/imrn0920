// Buatlah sebuah function dengan nama range() yang menerima dua parameter berupa number. Function mengembalikan sebuah array yang berisi angka-angka mulai dari angka parameter pertama hingga angka pada parameter kedua. Jika parameter pertama lebih besar dibandingkan parameter kedua maka angka-angka tersusun secara menurun (descending).

// struktur fungsinya seperti berikut range(startNum, finishNum) {}
// Jika parameter pertama dan kedua tidak diisi maka function akan menghasilkan nilai -1
console.log('= = = =  SOAL NOMOR 1 = = = = =')
function range (startNum, finishNum){
    var deret = [];
    if (startNum < finishNum){
        for (var i=startNum ;  i <= finishNum ; i++){
            deret.push(i);
        }

    }
    else if (startNum > finishNum){
        for (var i=startNum ; i >= finishNum ; i--){
            deret.push(i)
        }
    }
    else {
        var deret = '-1'
    }
    var deret = []
    var urut = deret.split(',')
return urut
}
console.log(range(1)) // Console Penjalan
/*
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
*/
console.log('= = = =  SOAL NOMOR 2 = = = = =')

function rangeWithStep (startNum, finishNum, step){
    var rangeArr = [];
    if (startNum > finishNum){
        var curretNum=startNum;
        for (var i=0 ;  curretNum >= finishNum ; i++){
            rangeArr.push(curretNum);
            curretNum-=step
        }

    }
    else if (startNum < finishNum){
        var curretNum=startNum;
        for (var i=0 ;  curretNum <= finishNum ; i++){
            rangeArr.push(curretNum);
            curretNum +=step
        }
    }
    else if(!startNum || !finishNum || step) {
        return -1 
    }
    return rangeArr
}
console.log (rangeWithStep(1,10,2))


// Pada soal kali ini kamu diminta membuat function rangeWithStep yang mirip dengan function range di soal sebelumnya namun parameternya ditambah dengan parameter ketiga yaitu angka step yang menyatakan selisih atau beda dari setiap angka pada array. Jika parameter pertama lebih besar dibandingkan parameter kedua maka angka-angka tersusun secara menurun (descending) dengan step sebesar parameter ketiga.

// struktur fungsinya seperti berikut rangeWithStep(startNum, finishNum, step) {}

// console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
// console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
// console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
// console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log('= = = =  SOAL NOMOR 3 = = = = =')


function sum (startNum, finishNum, step){
    var rangeArr= [];
    var distance ;

    if (!step) {
        distance=1
    }
    else{
        distance = step
        }
    if (startNum > finishNum){
        var curretNum = startNum;
        for (var i=0; curretNum >= finishNum; i++){
            rangeArr.push(curretNum)
            curretNum -= distance
        }
    }
    else if (startNum < finishNum){
        var curretNum = startNum;
        for (var i=0; curretNum <= finishNum; i++){
            rangeArr.push(curretNum)
            curretNum += distance
        }
    } 
    else if (!startNum && !finishNum && !step){
        return 0   
    } 
    else if (startNum){
        return startNum
    }
    
    var total = 0;
    for (var i=0; i<rangeArr.length;i++){
    total= total + rangeArr[i]
    }
    return total
}

console.log(sum(1,10)) // 621


// Kali ini kamu akan menjumlahkan sebuah range (Deret) yang diperoleh dari function range di soal-soal sebelumnya. Kamu boleh menggunakan function range dan rangeWithStep pada soal sebelumnya untuk menjalankan soal ini.
// Buatlah sebuah function dengan nama sum() yang menerima tiga parameter yaitu angka awal deret, angka akhir deret, dan beda jarak (step). Function akan mengembalikan nilai jumlah (sum) dari deret angka. contohnya sum(1,10,1) akan menghasilkan nilai 55.
// console.log(sum(1,10)) // 55
// console.log(sum(5, 50, 2)) // 621
// console.log(sum(15,10)) // 75
// console.log(sum(20, 10, 2)) // 90
// console.log(sum(1)) // 1
// console.log(sum()) // 0 



console.log('= = = =  SOAL NOMOR 4 = = = = =')

// Var Contoh
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling (data){
    var dataLenght = data.length
    for (var i=0 ; i<dataLenght; i++){
        var id = ' Nomor ID :' + data[i][0]
        var nama = 'Nama Lengkp : :' + data[i][1]
        var ttl = ' Tempat Tanggal Lahir :' + data[i][2] + " "+ data[i][3]
        var hobby = 'Hobby :' + data[i][4]
        
        console.log(id)
        console.log(nama)
        console.log(ttl)
        console.log(hobby)
        
    }
}
dataHandling(input)




// Buatlah sebuah fungsi dengan nama dataHandling dengan sebuah parameter untuk menerima argumen. 
// Argumen yang akan diterima adalah sebuah array yang berisi beberapa array sejumlah n. Contoh 
// input dapat dilihat dibawah:

//contoh input
// var input = [
//     ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
//     ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
//     ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
//     ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
// ] 


console.log('= = = =  SOAL NOMOR 5 = = = = =')

// Code di sini
// function balikKata (kata){
//     var kata=kata.length
//     for (i=kata-1 ; i<=kata ; i++){
//         console.log(kata)
//         return word
//     }

// }
function balikKata(kata) {
    var tampungKata = kata;
    var kataBaru = '';
   for (i = kata.length - 1; i >= 0; i--) {
    kataBaru = kataBaru + tampungKata[i];
    }
    return kataBaru;
   }
console.log(balikKata("Kasur Rusak"))


// console.log(balikKata("Kasur Rusak")) // kasuR rusaK
// console.log(balikKata("SanberCode")) // edoCrebnaS
// console.log(balikKata("Haji Ijah")) // hajI ijaH
// console.log(balikKata("racecar")) // racecar
// console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log('= = = =  SOAL NOMOR 6 = = = = =')

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]
dataHandling2(input);


function dataHandling2 (data){
    var dataplus = data;
    var namaplus = data[1] + 'Elss'
    var tempatplus = 'Provinsi' + data [2]
    var genderplus = "Pria"
    var sekolahplus = "SMA International Metro"

    dataplus.splice(1,1, namaplus)
    dataplus.splice(2,1, tempatplus)
    dataplus.splice(4,1, genderplus,sekolahplus)
    
    var arrDate = data[3]
    var datePlus = arrDate.split('/')
    var monthNum = datePlus [1]
    var monthname = " "

    switch (monthNum){
        case "01":
        monthname= "Januari"
        break;
        case "02":
        monthname= "Februari"
        break;
        case "03":
        monthname= "Maret"
        break;
        case "04":
        monthname= "April"
        break;
        case "05":
        monthname= "Mei"
        break;
        case "06":
        monthname= "Juni"
        break;
        case "07":
        monthname= "Juli"
        break;
        case "08":
        monthname= "Agustus"
        break;
        case "09":
        monthname= "September"
        break;
        case "10":
        monthname= "Oktober"
        break;
        case "11":
        monthname= "November"
        break;
        case "12":
        monthname= "Desember"
        break; 
        default:
        break;
    }
    var dateJoin = datePlus.join("-")
    var arrDate = dataplus.sort(function(value1,value2){
        value2 - value1
    })
    var editName = namaplus.slice(0,15)
    console.log(dataplus)

    console.log(monthname)
    console.log(arrDate)
    console.log(dateJoin)
    console.log(editName)

    
}

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */
function myApp(){
    var total = 5;
    var output = "";
    for (var i=1; i<=total ; i++){
        for (var j=1;j<=i;j++){
            output +=j + " ";
        }
        console.log(output);
        output="";
    }
}
myApp()