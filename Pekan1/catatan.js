// Function sederhana tanpa return
function tampilkan() {
    console.log("halo!");
  }
   
  tampilkan(); 
console.log('-------------------------------------------------')
// : Function sederhana dengan return
  function munculkanAngkaDua() {
    return 2
  }

  console.log('-------------------------------------------------')
// : Function dengan parameter
  var tampung = munculkanAngkaDua();
  console.log(tampung)

  function kalikanDua(angka) {
    return angka * 2
  }
   
  var tampung = kalikanDua(2);
  console.log(tampung) 
console.log('-------------------------------------------------')
//: Pengiriman parameter lebih dari satu
function tampilkanAngka(angkaPertama, angkaKedua) {
    return angkaPertama + angkaKedua
  }
   
  console.log(tampilkanAngka(5, 3))

console.log('-------------------------------------------------')
//  Inisialisasi parameter dengan nilai default
  function tampilkanAngka(angka = 1) {
    return angka
  }
   
  console.log(tampilkanAngka(5)) // 5, sesuai dengan nilai parameter yang dikirim
  console.log(tampilkanAngka()) // 1, karena default dari parameter adalah 1
  


  var fungsiPerkalian = function(angkaPertama, angkaKedua) {
    return angkaPertama * angkaKedua
  }
   
  console.log(fungsiPerkalian(2, 4))
  