// Universal Variable //
var pembatas = '______________________________________________' ;
//
console.log(pembatas);
console.log ('Soal Nomor 1');
/* Tugas String
Soal No. 1 (Membuat kalimat),
Terdapat kumpulan variable dengan data string sebagai berikut

// var word = 'JavaScript'; 
//    var second = 'is'; 
//    var third = 'awesome'; 
//   var fourth = 'and'; 
//   var fifth = 'I'; 
//    var sixth = 'love'; 
//   var seventh = 'it!';

Buatlah agar kata-kata di atas menjadi satu kalimat . Output:
JavaScript is awesome and I love it! */

// ++ Jawaban NO.1 ++ //
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love';
var seventh = 'it!';
//Extra Var Spasi//
var s = ' ';
console.log(word + s + second + s + third + s + fourth + s + fifth  + s +sixth + s + seventh)
console.log(pembatas);
//_____________________________________________________________________________
/*
Soal No.2 Mengurai kalimat (Akses karakter dalam string)

var sentence = "I am going to be React Native Developer"; 

var exampleFirstWord = sentence[0] ; 
var exampleSecondWord = sentence[2] + sentence[3]  ; 
var thirdWord; // lakukan sendiri 
var fourthWord; // lakukan sendiri 
var fifthWord; // lakukan sendiri 
var sixthWord; // lakukan sendiri 
var seventhWord; // lakukan sendiri 
var eighthWord; // lakukan sendiri 

console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord)

Buatlah menjadi Output

First word: I 
Second word: am 
Third word: going 
Fourth word: to 
Fifth word: be 
Sixth word: React 
Seventh word: Native 
Eighth word: Developer
*/

// ++ Jawaban NO.2 ++ //
console.log ('Soal Nomor 2');
var sentence = "I am going to be React Native Developer"; 

var FirstWord = sentence[0] ; 
var SecondWord = sentence[2] + sentence[3]  ; 
var ThirdWord = sentence [5] + sentence [6] + sentence [7] + sentence [8] + sentence [9];
var ForthWord = sentence [11] + sentence [12];
var FifthWord = sentence [14] + sentence [15] ; 
var SixthWord = sentence [17]+ sentence [18] + sentence [19] + sentence [20]+ sentence [21];
var SeventhWord = sentence [23]+ sentence [24] + sentence [25] + sentence [26]+ sentence [27]+ sentence [28];
var EighthWord = sentence [30] + sentence [31] + sentence [32] + sentence [33] + sentence [34]+ sentence [35]+ sentence [36] + sentence [37]+ sentence [38];

console.log('First Word: ' + FirstWord); 
console.log('Second Word: ' + SecondWord); 
console.log('Third Word: ' + ThirdWord);
console.log('Forth Word: ' + ForthWord);
console.log('Fifth Word: ' + FifthWord);
console.log('Sixth Word: ' + SixthWord);
console.log('Seventh Word: ' + SeventhWord);
console.log('Eighth Word: ' + EighthWord); 


console.log(pembatas);
//_____________________________________________________________________________
/*
Soal No. 3 Mengurai Kalimat (Substring)

var sentence2 = 'wow JavaScript is so cool'; 

var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2; // do your own! 
var thirdWord2; // do your own! 
var fourthWord2; // do your own! 
var fifthWord2; // do your own! 

console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);

Uraikan lah kalimat sentence2 di atas menjadi kata-kata penyusunnya. Output:

First Word: wow 
Second Word: JavaScript 
Third Word: is 
Fourth Word: so 
Fifth Word: cool
*/

// ++ Jawaban NO.3 ++ //
console.log ('Soal Nomor 3');
var sentence2 = 'wow JavaScript is so cool'; 

var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 14);
var thirdWord2 = sentence2.substring(15, 17);
var fourthWord2 = sentence2.substring(18, 20);
var fifthWord2 = sentence2.substring(21, 25);

console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);

console.log(pembatas);
// Soal Nomor 4

/*
First Word: wow, with length: 3 
Second Word: JavaScript, with length: 10 
Third Word: is, with length: 2 
Fourth Word: so, with length: 2 
Fifth Word: cool, with length: 4
*/
// ++ Jawaban NO.4 ++ //
console.log ('Soal Nomor 4');
var sentence3 = 'wow JavaScript is so cool'; 

var pertama1 = sentence3.substring(0, 3); 
var kedua1 = sentence3.substring(4, 14); 
var ketiga1 = sentence3.substring(15, 17); 
var keempat1 = sentence3.substring(18, 20); 
var kelima1 = sentence3.substring(20, 24); 

var pertama1 = pertama1.length  
var kedua1 = kedua1.length  
var ketiga1 = ketiga1.length  
var keempat1 = keempat1.length  
var kelima1 = kelima1.length  

console.log('First Word: ' + pertama1 + ', with length: ' + pertama1); 
console.log('Second Word: ' + kedua1 + ', with length: ' + kedua1); 
console.log('Third Word: ' + ketiga1 + ', with length: ' + ketiga1); 
console.log('Fourth Word: ' + keempat1 + ', with length: ' + keempat1);  
console.log('Fifth Word: ' + kelima1 + ', with length: ' + kelima1); 