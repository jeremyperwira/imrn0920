// Looping Pertama //
console.log('LOOPING PERTAMA')

var deret = 2 
var start = 0;
while(start < 20) {
  start += deret;
  console.log (start + ' - I love coding')
}

// - - - - - - - - - - - - - - - - - - //
// Looping Kedua //
console.log('LOOPING KEDUA')

for(var i=1;i<=20;i+=1){
  if(i%2==1){
      if(i%3==0){
          console.log(i +" - I Love Coding");
      }else{
          console.log(i +" - Santai");
      }
  }else{
      console.log(i +" - Berkualitas");
  }
}
// - - - - - - - - - - - - - - - - - - //
//Kamu diminta untuk menampilkan persegi dengan dimensi 8×4 dengan tanda pagar (#) dengan
//perulangan atau looping. Looping boleh menggunakan syntax apa pun (while, for, do while).

console.log('<= = = = = = = = = = = = = MEMBUAT PERSEGI = = = = = = = = = = = = =>')
for(var panjang=1 ;panjang <= 4; panjang++)
console.log("########")
// - - - - - - - - - - - - - - - - - - //

// - - - - - - - - - - - - - - - - - - //
console.log('<= = = = = = = = = = = MEMBUAT PAPAN CATUR = = = = = = = = = = =>')
for(var catur=1 ;catur <= 8; catur++){
  if( catur%2==1){
  console.log("# # # #")
}
  else console.log(" # # # #")}
  
// - - - - - - - - - - - - - - - - - - //
/*
while([Kondisi]) { // Kondisi yang menentukan apakah program akan melakukan iterasi. 
    // Berupa boolean atau true/false.
      [Proses] // Merupakan proses yang akan dijalankan dalam satu iterasi
    }
*/
// While Loop //
/*
var flag = 1;
while(flag < 10) { // Loop akan terus berjalan selama nilai flag masih dibawah 10
  console.log('Iterasi ke-' + flag); // Menampilkan nilai flag pada iterasi tertentu
  flag++; // Mengubah nilai flag dengan menambahkan 1
}


var deret = 5;
var jumlah = 0;
while(deret > 0) { // Loop akan terus berjalan selama nilai deret masih di atas 0
  jumlah += deret; // Menambahkan nilai variable jumlah dengan angka deret
  deret--; // Mengubah nilai deret dengan mengurangi 1
  console.log('Jumlah saat ini: ' + jumlah)
}
 
console.log(jumlah);

console.log('/=====\/=====\/=====\/=====\/=====\/=====\/=====\/=====\/=====\ ')

//For Loop //
/////////////////////////////////////////////////////////////////////////////////
console.log('Contoh Looping For-loop 1 Looping Angka 1-9 Sederhana')
for(var angka = 1; angka < 10; angka++) {
    console.log('Iterasi ke-' + angka);
  } 
  console.log(' ')
/////////////////////////////////////////////////////////////////////////////////
  console.log('Contoh Looping For-loop 2 Looping Mengembalikan Angka Total')
  var jumlah = 0;
for(var deret = 5; deret > 0; deret--) {
  jumlah += deret;
  console.log('Jumlah saat ini: ' + jumlah);
}
console.log('Jumlah terakhir: ' + jumlah);
console.log(' ')
/////////////////////////////////////////////////////////////////////////////////
console.log('Contoh Looping For-loop 3 Looping Dengan Increment dan Decrement Lebih dari 1')
for(var deret = 0; deret < 10; deret += 2) {
    console.log('Iterasi dengan Increment counter 2: ' + deret);
  }
   
  console.log('-------------------------------');
   
  for(var deret = 15; deret > 0; deret -= 3) {
    console.log('Iterasi dengan Decrement counter : ' + deret);
  } 
  console.log(' ')

// Awas Inifinity Loop , hanya untuk pebelajaran tidak untuk di run// 
/*
var flag = 1;
while(flag < 10) { 
  console.log('Iterasi ke-' + flag);
}
*/
